//Yandex Ecommerce
function sendYandexEcommerce(array, action) {
  if(typeof dataLayer == 'undefined')
      return false;

  var product = [{
    "id": array['id'],
    "name": array['name'],
    "price": array['price'],
    "category": array['category'],
    "quantity": array['quantity'],
    "variant" : array['variant']
  }];

  switch (action) {
    case "add": dataLayer.push({"ecommerce": {"currencyCode": "RUB", "add": {"products": product}}}); break;
    case "remove": dataLayer.push({"ecommerce": {"currencyCode": "RUB", "remove": {"products": product}}}); break;
    case "detail":
      delete product[0]["quantity"];
      dataLayer.push({"ecommerce": {"currencyCode": "RUB", "detail": {"products": product}}});
      break;
  }
}

$(document).ready(function(){

  $('.sort-block .btn-sort').click(function() {
    $(this).toggleClass('active');
    $(this).next().toggleClass('active');
  });

  function restrict_value(obj) {
    obj.value=Math.max(Math.min(obj.value, obj.max), obj.min);
  }

  function set_price(obj, price, quantity) {
    obj.text(new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'RUB' }).format(price * quantity));
  }

  $('.btn-minus').each(function() {
    $(this).click(function() {
      $(this).next().val(function(i, oldval) {
        return (oldval - 1 < 1 ? 1 : --oldval);
      }).change();
      var price = $(this).parents('.product-js').find('.price-js');
      set_price(price, parseFloat(price.attr('data-price')), $(this).next().val());
    });
  });

  $('.btn-plus').each(function() {
    $(this).click(function() {
      $(this).prev().val(function(i, oldval) {
        return ++oldval;
      }).change();
      var price = $(this).parents('.product-js').find('.price-js');
      set_price(price, parseFloat(price.attr('data-price')), $(this).prev().val());
    });
  });

  $('input[name=quantity]').each(function() {
    var ts;
    $(this).on('focusout', function() {
      $(this).val(function(i, oldval) {
        return (oldval < 1 ? 1 : oldval);
      }).change();
      var price = $(this).parents('.product-js').find('.price-js');
      set_price(price, parseFloat(price.attr('data-price')), $(this).val());
    }).trigger('focusout').on('mousewheel', function(event) {
      if (event.deltaY > 0) {
        $(this).next().click();
      } else {
        $(this).prev().click();
      }
      event.preventDefault();
    });
  });
});