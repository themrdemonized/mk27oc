<?php
class ControllerInformationAbout extends Controller {
	public function index() {
		$this->document->setTitle('О компании');
		$this->document->setDescription('Мы специализируемся на поставке метизов, крепежа, грузоподъемного оборудования, инженерных систем. Компания «Метиз Комплект» удовлетворяет потребности самых разных клиентов – от розничных покупателей до крупных производителей и строительных организаций. Продукция, предоставляемая компанией, используется практически во всех отраслях промышленности: авиационной, электротехнической, химической, автомобилестроении, строительной отрасли, сельском хозяйстве');
		$this->document->setKeywords('');

		$data['city'] = isset($this->session->data['city']) ? $this->session->data['city'] : NULL;

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('information/about', $data));
	}
}