<?php
class ControllerInformationDevelop extends Controller {
	public function index() {
		$this->document->setTitle('Клиентам');
		$this->document->setDescription('Производим металлоизделия, изготовленные по ГОСТу, ТУ и ОСТ, и по вашим чертежам');
		$this->document->setKeywords('металлоизделия,ГОСТ,ТУ,ОСТ,чертежи');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('information/develop', $data));
	}
}