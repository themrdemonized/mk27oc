<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerCommonCity extends Controller {
	public function index() {
		$json = array();

		if (isset($this->request->post['city'])) {
			$this->session->data['city'] = $this->request->post['city'];
			$json['city'] = $this->request->post['city'];
			$this->session->data['guest']['customer_group_id'] = isset($this->request->post['city_id']) ? $this->request->post['city_id'] : $this->session->data['guest']['customer_group_id'];
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
