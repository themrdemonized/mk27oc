<?php
// Heading
$_['page_title']    = 'Файлменеджер галереи';
$_['heading_title']    = '<a href="https://prowebber.ru/" target="_blank" title="ProWebber" style="color:#0362B6;margin-right:5px"><i class="fa fa-cloud-download fa-fw"></i></a>  '. $_['page_title'];

// Text
$_['text_uploaded']    = 'Успех: ваш файл загружен!';
$_['text_directory']   = 'Успех: созданный каталог!';
$_['text_delete']      = 'Успех: ваш файл или каталог удален!';

// Entry
$_['entry_search']     = 'Поиск..';
$_['entry_folder']     = 'Название папки';

// Error
$_['error_permission'] = 'Предупреждение: недостаточно прав!';
$_['error_filename']   = 'Внимание: имя файла должно быть от 3 до 255!';
$_['error_folder']     = 'Внимание: имя папки должно быть от 3 до 255!';
$_['error_exists']     = 'Предупреждение: файл или каталог с тем же именем уже существует!';
$_['error_directory']  = 'Предупреждение: Каталог не существует!';
$_['error_filetype']   = 'Предупреждение: неправильный тип файла!';
$_['error_upload']     = 'Внимание: файл нельзя загрузить по неизвестной причине!';
$_['error_delete']     = 'Предупреждение: вы не можете удалить этот каталог!';