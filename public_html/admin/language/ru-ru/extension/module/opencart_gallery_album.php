<?php
// Heading
$_['page_title']       = 'Галерея: Рекомендуемые фотоальбомы';
$_['heading_title']    = '<a href="https://prowebber.ru/" target="_blank" title="ProWebber" style="color:#0362B6;margin-right:5px"><i class="fa fa-cloud-download fa-fw"></i></a>  '. $_['page_title'];

// Text
$_['text_module']         = 'Модули';
$_['text_success']        = 'Успешно: Вы изменили модуль Галерея Фотоальбомы!';
$_['text_content_top']    = 'Верх страницы';
$_['text_content_bottom'] = 'Низ страницы';
$_['text_column_left']    = 'Левая колонка';
$_['text_column_right']   = 'Правая колонка';
$_['text_edit']           = 'Редактирование фотогалереи';

// Entry
$_['entry_limit']         = 'Лимит:';
$_['entry_apr']           = 'Альбомов в строке:';
$_['entry_as']            = 'Размер альбома:';    
$_['entry_layout']        = 'Макет:';
$_['entry_position']      = 'Позиция:';
$_['entry_status']        = 'Статус:';
$_['entry_sort_order']    = 'Порядок:';
$_['entry_sort_by']       = 'Сортировка:';
$_['entry_album']         = 'Рекомендуемый альбом:';
$_['entry_image']      = 'Картинка (Ш x В)';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';

// Help
$_['help_album']     = '(Автозаполнение)';

// Error
$_['error_permission']    = 'Внимание: У вас недостаточно прав на управление модулем!';
$_['error_image']         = 'Ширина и высота картинки обязательно!';
?>