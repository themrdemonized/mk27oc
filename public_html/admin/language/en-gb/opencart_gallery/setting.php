<?php
// Heading
$_['page_title']                = 'Opencart Gallerry Settings';
$_['heading_title']    = '<a href="https://prowebber.ru/" target="_blank" title="ProWebber" style="color:#0362B6;margin-right:5px"><i class="fa fa-cloud-download fa-fw"></i></a>  '. $_['page_title'];

$_['tab_setting']             	   = 'Settings';
$_['tab_newscatpage']              = 'News Category Page';
$_['tab_newspage']             	   = 'News Page';

$_['text_rectangular']             = 'Rectangular';
$_['text_square']                  = 'Square';

$_['entry_picture_type']           = 'News Picture Type:';
$_['entry_meta_length']            = 'Meta Description Length:';
$_['entry_column_width']           = 'New Category Column Width:';
$_['entry_column_height']          = 'New Category Column Height:';

$_['entry_limit_category']         = 'Default Items Per Page (News):';
$_['entry_limit_per_row']         = 'Default Items Per Row (News):';


$_['text_success']                 = 'Success: You have modified news settings!';



// Text

$_['text_items']                   = 'Items';
$_['text_product']                 = 'Products';
$_['text_voucher']                 = 'Vouchers';
$_['text_tax']                     = 'Taxes';
$_['text_account']                 = 'Account';
$_['text_checkout']                = 'Checkout';
$_['text_stock']                   = 'Stock';
$_['text_affiliate']               = 'Affiliates';
$_['text_return']                  = 'Returns';
$_['text_image_manager']           = 'Image Manager';
$_['text_browse']                  = 'Browse';
$_['text_clear']                   = 'Clear';
$_['text_shipping']                = 'Shipping Address';
$_['text_payment']                 = 'Payment Address';
$_['text_mail']                    = 'Mail';
$_['text_smtp']                    = 'SMTP';

// Error
$_['error_warning']                = 'Warning: Please check the form carefully for errors!';
$_['error_permission']             = 'Warning: You do not have permission to modify settings!';
?>
